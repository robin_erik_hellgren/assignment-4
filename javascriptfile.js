
let bankBalance = 200;
let loanAmount = 0;
let numberOfLoan = 0;
let workPay = 0;
let laptops = [];

let bankBalanceElement = document.getElementById("bankBalance");
let loanBalanaceElement = document.getElementById("loanBalance");
let payALoanButton = document.getElementById("payALoanButton");
let payWorkBackLoanButton = document.getElementById("payWorkBackLoanButton");
let displayTheBankElement = document.getElementById("displayTheBank");
let displayWorkElement = document.getElementById("displayWork");
let displayLaptopsElement = document.getElementById("displayLaptops");
let workPayElement = document.getElementById("workPay");
let laptopsElement = document.getElementById("laptops");
let laptopsInfoElement = document.getElementById("laptopsInformation")
let laptopsExtendedInfoElement = document.getElementById("laptopsExtendedInformation")

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToStore(laptops))
    .catch(function(error){
        console.log(error)
    });

const addLaptopsToStore = (laptops) => {
    laptops.forEach(x => addLaptopToStore(x));
}

const addLaptopToStore = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.innerText = laptop.title;
    laptopElement.value = laptop.id; 
    laptopsElement.add(laptopElement);
}

///Function for buying a laptop - checks current balance - if there is enough it deducts the price.
function buyLaptop(laptopPrice){
    if(laptopPrice<=bankBalance){
        bankBalance=bankBalance-laptopPrice;
        window.alert("You have bought a new laptop!");
    }
    else {
        window.alert("You cannot afford that laptop!");
    }
}

const handleLaptopMenuChange = e => {
    laptopsExtendedInfoElement.style.display = "block";
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopsInfoElement.innerHTML = "<br/><ul><ut>Features:</ut>" + listLaptopSpecs(selectedLaptop.specs).join(" ") + "</ul>";
    laptopsExtendedInfoElement.innerHTML = "<br/><img src='https://noroff-komputer-store-api.herokuapp.com/" 
        + selectedLaptop.image 
        + "'/><br/>" 
        + selectedLaptop.title 
        + "<br/>" 
        + selectedLaptop.description 
        + "<br/>" 
        + selectedLaptop.price 
        + "kr"
        + "<br/>"
        + "<button id='buyLaptopButton' onclick='buyLaptop("+selectedLaptop.price+")'>Buy Now</button><br/>";
    }

/// Function wrapping the presented array values in HTML list tags.
function listLaptopSpecs(array){
    for (a=0;a<array.length;a++){
        if(array[a].includes("<li>")){
            return array;
        }   
        else{ 
            if(array[a] != null){
                array[a] = "<li>" + array[a] + "</li>";
            }
        }
    }
    return array;
}

///Function that shows the bank balance and the loan - if there is one.
function displayBalance() {
    if (loanAmount == 0) {
        bankBalanceElement.innerHTML = "Bank balance: " + bankBalance + "kr";
        loanBalanaceElement.innerHTML = "";
        numberOfLoan = 0;
        payALoanButton.style.display = "none";
        payWorkBackLoanButton.style.display = "none";
    }
    else {
        bankBalanceElement.innerHTML = "Bank balance: " + bankBalance + "kr";
        loanBalanaceElement.innerHTML = "Outstandning loan: " + loanAmount + "kr";
        payALoanButton.style.display = "inline";
        payWorkBackLoanButton.style.display = "inline";
    }
}

///Function for getting a loan - checks if the user already got a loan and checks the input values - then gives the loan and calls displayBalance to update the balance.
function getALoan() {
    if (numberOfLoan == 0) {
        let checkLoanAmount = window.prompt("How much do you want?", "")
        if (isNaN(checkLoanAmount)) {
            window.alert("Ask for a number instead!")
        }
        else if (checkLoanAmount == null){

        }
        else {
            checkLoanAmount = parseInt(checkLoanAmount);

            if (checkLoanAmount > (bankBalance * 2) + 1 || bankBalance == 0) {
                window.alert("That's too much!")
            }
            else if (confirm) {
                window.alert("You got it!")
                bankBalance = checkLoanAmount + bankBalance;
                loanAmount = checkLoanAmount;
                numberOfLoan = 1;
                displayBalance();
            }
        }
    }
    else {
        window.alert("You already got a loan!")
    }
}

///Function for paying a loan - checks for correct input then checks if the balance is enough.

function payALoan() {
    let payLoanAmount = window.prompt("How much do you want to pay back?", "")
    if (isNaN(payLoanAmount) || payLoanAmount == undefined || payLoanAmount == null || payLoanAmount == 0) {
        window.alert("Insert a number!");
    }
    else if (payLoanAmount > bankBalance) {
        window.alert("You don't have that much!")
    }
    else if (payLoanAmount > loanAmount) {
        window.alert("That's too much!");
    }
    else {
        payLoanAmount = parseInt(payLoanAmount);
        loanAmount = loanAmount - payLoanAmount;
        bankBalance = bankBalance - payLoanAmount;
        window.alert("Thank you!");
        displayBalance();
    }
}

///Displays the gathered pay from the work function.
function displayPay() {
    workPayElement.innerHTML = "Pay: " + workPay + "kr";
}

///Function for doing work - adding pay to balance.
function doSomeWork() {
    workPay = workPay + 100;
    workPayElement.style.display = "block";
    displayPay();
}

///Function for putting money from work to the bank. Deducts 10% towards the loan if there is one.
function putInBank() {
    bankBalance = bankBalance + (0.9 * workPay);
    if ((0.1 * workPay) > loanAmount) {
        bankBalance = bankBalance + (0.1 * workPay) - loanAmount;
        loanAmount = 0;
    }
    else {
        loanAmount = loanAmount - (0.1 * workPay);
    }
    workPay = 0;
    displayBalance();
    displayPay();
}

///Function for paying back the loan from Work Pay before it is banked.
function payWorkBackLoan() {
    if (workPay > loanAmount) {
        workPay = workPay - loanAmount;
        loanAmount = 0;
        displayPay();
        displayBalance();
    }
    else {
        loanAmount = loanAmount - workPay;
        workPay = 0;
        displayPay();
        displayBalance();
    }
}

///Function for the bank navigation button.
function displayTheBank() {
    if (displayTheBankElement.style.display == "none") {
        displayTheBankElement.style.display = "block";
        displayWorkElement.style.display = "none";
        displayLaptopsElement.style.display = "none";
        laptopsExtendedInfoElement.style.display = "none";
        theBankButton.style.backgroundColor = "rgb(96, 184, 88)";
        workButton.style.backgroundColor = "rgb(218, 223, 199)";
        laptopsButton.style.backgroundColor = "rgb(218, 223, 199)";
    }
    else {
        displayTheBankElement.style.display = "none";
        theBankButton.style.backgroundColor = "rgb(218, 223, 199)";
    }
}

///Function for the work navigation button.
function displayWork() {
    if (displayWorkElement.style.display == "none") {
        displayTheBankElement.style.display = "none";
        displayWorkElement.style.display = "block";
        displayLaptopsElement.style.display = "none";
        laptopsExtendedInfoElement.style.display = "none";
        theBankButton.style.backgroundColor = "rgb(218, 223, 199)";
        workButton.style.backgroundColor = "rgb(96, 184, 88)";
        laptopsButton.style.backgroundColor = "rgb(218, 223, 199)";
    }
    else {
        displayWorkElement.style.display = "none";
        workButton.style.backgroundColor = "rgb(218, 223, 199)";
    }
}

///Function for the laptops navigation button.
function displayLaptops() {

    if (displayLaptopsElement.style.display == "none") {
        displayTheBankElement.style.display = "none";
        displayWorkElement.style.display = "none";
        displayLaptopsElement.style.display = "block";
        theBankButton.style.backgroundColor = "rgb(218, 223, 199)";
        workButton.style.backgroundColor = "rgb(218, 223, 199)";
        laptopsButton.style.backgroundColor = "rgb(96, 184, 88)";
    }
    else {
        displayLaptopsElement.style.display = 'none';
        laptopsExtendedInfoElement.style.display = "none";
        laptopsButton.style.backgroundColor = "rgb(218, 223, 199)";
    }
}

displayLaptopsElement.addEventListener("onclick", handleLaptopMenuChange);
laptopsElement.addEventListener("change", handleLaptopMenuChange);